import glossary.Glossary;
import org.apache.commons.lang3.time.StopWatch;
import utils.Utils;

import java.io.*;

public class Main {

    public static void main(String[] args) throws IOException {

        //taking execution time
        StopWatch stopwatch = new StopWatch();
        stopwatch.start();

        Utils utils = new Utils();

        Glossary glossary = utils.createFilledGlossary("ISTQB_glossary.txt");

        glossary.addAspects();
        glossary.addAspectGraphNodes();
        glossary.addAspectGraphEdges();
        glossary.reduceAspectGraphRelations();
        glossary.exportAspectGraphsToDOT("output");


        stopwatch.stop();
        long timeTaken = stopwatch.getTime();
        System.out.println("\nEnd!\n");
        System.out.println("Time taken: " + timeTaken);
    }

}
