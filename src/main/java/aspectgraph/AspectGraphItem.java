package aspectgraph;

import glossary.GlossaryItem;

import java.util.ArrayList;
import java.util.List;

public class AspectGraphItem {
    private GlossaryItem glossaryItem;
    private List<AspectGraphItem> linksTo = new ArrayList<>();

    public GlossaryItem getGlossaryItem() {
        return glossaryItem;
    }

    public void setGlossaryItem(GlossaryItem glossaryItem) {
        this.glossaryItem = glossaryItem;
    }

    public List<AspectGraphItem> getLinksTo() {
        return linksTo;
    }

    public void setLinksTo(List<AspectGraphItem> linksTo) {
        this.linksTo = linksTo;
    }
}
