package glossary;

import aspectgraph.AspectGraphItem;
import edu.stanford.nlp.ie.util.RelationTriple;
import edu.stanford.nlp.ling.CoreAnnotations;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.ling.IndexedWord;
import edu.stanford.nlp.naturalli.NaturalLogicAnnotations;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.semgraph.SemanticGraph;
import edu.stanford.nlp.semgraph.SemanticGraphCoreAnnotations;
import edu.stanford.nlp.simple.Sentence;
import edu.stanford.nlp.trees.TypedDependency;
import edu.stanford.nlp.util.CoreMap;
import org.apache.commons.text.CaseUtils;
import utils.Utils;


import java.io.*;
import java.util.*;
import java.util.stream.Collectors;

public class Glossary {
    private static final int ASPECT_COUNT = 10;
    //constant used for searching in definition (first N words)
    private final int N = 1;

    private Map<String, GlossaryItem> terms = new TreeMap<>(String.CASE_INSENSITIVE_ORDER);

    private List<String> aspects = new ArrayList<>();

    //Map with key=aspectName, value=aspectGraph
    private Map<String, List<AspectGraphItem>> aspectGraphs = new HashMap<>();

    public Glossary() {
    }

    public Map<String, GlossaryItem> getTerms() {
        return terms;
    }

    public void setTerms(Map<String, GlossaryItem> terms) {
        this.terms = terms;
    }

    public void addAspects() throws FileNotFoundException {
        Properties props = new Properties();

        props.setProperty("annotators", "tokenize, ssplit, pos, lemma, depparse");

        StanfordCoreNLP pipeline = new StanfordCoreNLP(props);
        //iterate glossary and add potential links to terms
        Map<String, Double> wordWeight = new HashMap<>();
        for (Map.Entry<String, GlossaryItem> entry : this.getTerms().entrySet()) {

            String text = entry.getValue().getDefinition()+ " is " + entry.getValue().getDefinition();
            Set<String> potentialLinks = new HashSet<>();

            Annotation document = new Annotation(text);
            pipeline.annotate(document);

            List<CoreMap> sentences = document.get(CoreAnnotations.SentencesAnnotation.class);
            for(CoreMap sentence: sentences) {

                // this is the Stanford dependency graph of the current sentence
                SemanticGraph dependencies = sentence
                        .get(SemanticGraphCoreAnnotations.CollapsedCCProcessedDependenciesAnnotation.class);
                //find weight of all words in glossary
                for (IndexedWord iw : dependencies.vertexSet()) {
                    if (iw.get(CoreAnnotations.PartOfSpeechAnnotation.class).contains("NN")) { //if given word is kind of noun
                        String word = iw.lemma();
                        Double currentWordWeight = 1 + Math.pow(2, -(dependencies.getPathToRoot(iw).size()));
                        if (wordWeight.containsKey(word))
                            wordWeight.put(word, wordWeight.get(word) + currentWordWeight);
                        else
                            wordWeight.put(iw.word(), currentWordWeight);
                    }
                }

            }
            System.out.println("done: " + entry.getKey());
        }
        setAspects(wordWeight);

    }

    private void setAspects(Map<String, Double> wordWeight) throws FileNotFoundException {
        Utils utils = new Utils();
        Set<String> stopWords = utils.createSetFromFile("stopwords.txt");



        wordWeight.keySet().removeAll(stopWords);
        wordWeight.entrySet()
                .stream()
                .sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
                .limit(ASPECT_COUNT)
                .map(Map.Entry::getKey)
                .forEach(this.aspects::add);
    }

    public void addAspectGraphNodes(){
        for (String aspectName : this.aspects) {
            addSingleAspectGraphNodes(aspectName);
        }
    }

    private void addSingleAspectGraphNodes(String aspectName){
        List<AspectGraphItem> aspectGraph = new ArrayList<>();

        Properties props = new Properties();
        props.setProperty("annotators", "tokenize, ssplit, pos, lemma");
        StanfordCoreNLP pipeline = new StanfordCoreNLP(props);
        Sentence sentence;
        for (GlossaryItem glossItem : this.terms.values()) {

            //lemmatized term
            sentence = new Sentence(glossItem.getTerm());
            List<String> termLemmas= sentence.lemmas();

            //lemmatized beginning of definition
            sentence = new Sentence(glossItem.getDefinition());
            List<String> DefinitionFirstNLemmas = sentence.lemmas().stream().limit(N).collect(Collectors.toList());

            if (termLemmas.contains(aspectName) || DefinitionFirstNLemmas.contains(aspectName)
                || Utils.getWords(glossItem.getSeeAlso()).contains(aspectName)
                || Utils.getWords(glossItem.getSynonyms()).contains(aspectName)) {
                AspectGraphItem aspectGraphItem = new AspectGraphItem();
                aspectGraphItem.setGlossaryItem(glossItem);
                aspectGraph.add(aspectGraphItem);
            }

        }

        this.aspectGraphs.put(aspectName, aspectGraph);
    }

    public void addAspectGraphEdges(){
        for (Map.Entry<String, List<AspectGraphItem>> entry : this.aspectGraphs.entrySet()) {
            addSingleAspectGraphEdges(entry.getKey(), entry.getValue());
        }
    }

    private void addSingleAspectGraphEdges(String aspectName, List<AspectGraphItem> aspectGraph){
        GlossaryItem rootGlossItem = new GlossaryItem();
        rootGlossItem.setTerm(aspectName + " (aspect)");
        AspectGraphItem root = new AspectGraphItem();
        root.setGlossaryItem(rootGlossItem);
        //by default aspect node has edge to all other nodes
        root.getLinksTo().addAll(aspectGraph);

        for (AspectGraphItem node1 : aspectGraph) {
            for (AspectGraphItem node2 : aspectGraph) {
                if (node1 != node2){
                    String node2FirstNWords = Arrays.stream(node2.getGlossaryItem().getDefinition().split(" "))
                            .limit(N).collect(Collectors.joining(" "));
                    if (node2.getGlossaryItem().getTerm().contains(node1.getGlossaryItem().getTerm()) ||
                        node2FirstNWords.contains(node1.getGlossaryItem().getTerm()) ||
                        node2.getGlossaryItem().getSeeAlso().contains(node1.getGlossaryItem().getTerm())){
                            node1.getLinksTo().add(node2);
                    }
                }
            }
        }

        aspectGraph.add(root);
    }

    public void reduceAspectGraphRelations(){
        for (Map.Entry<String, List<AspectGraphItem>> entry : this.aspectGraphs.entrySet()) {
            reduceSingleAspectGraphRelations(entry.getValue());
        }
    }

    private void reduceSingleAspectGraphRelations(List<AspectGraphItem> aspectGraph){
        for (AspectGraphItem node1 : aspectGraph) {
            for (AspectGraphItem node2 : aspectGraph) {
                if (node1 != node2) {
                    if (existsIndirectPathFromTo(node1, node2)){
                        node1.getLinksTo().remove(node2);
                    }
                }
            }
        }
    }

    private Boolean existsIndirectPathFromTo(AspectGraphItem node1, AspectGraphItem node2){
        Set<AspectGraphItem> visitedNodes = new HashSet<>();
        visitedNodes.add(node1);

        Boolean result = false;
        for (AspectGraphItem node3 : node1.getLinksTo()) {
            visitedNodes.add(node3);
            result = result || existsIndirectPathFromTo(node3, node2, visitedNodes);
        }

        return result;
    }
    private Boolean existsIndirectPathFromTo(AspectGraphItem node1, AspectGraphItem node2,
                                                                    Set<AspectGraphItem> visitedNodes){
        if (node1.getLinksTo().contains(node2))
            return true;

        Boolean result = false;
        for (AspectGraphItem node3 : node1.getLinksTo()) {
            if (!visitedNodes.contains(node3)) {
                visitedNodes.add(node3);
                result = result || existsIndirectPathFromTo(node3, node2, visitedNodes);
            }
        }
        return result;

    }


    public void exportAspectGraphsToDOT(String directory) throws IOException {
        File folder = new File(directory);
        if (!folder.exists())
            folder.mkdir();
        else{
            List<File> fList = Arrays.asList(folder.listFiles());
            for (File file : fList) {
                if (file.getName().endsWith(".dot"))
                    file.delete();
            }
        }

        for (Map.Entry<String, List<AspectGraphItem>> entry : this.aspectGraphs.entrySet()) {
            exportAspectGraphToDot(directory, entry.getKey(), entry.getValue());
        }
    }

    private void exportAspectGraphToDot(String directory, String aspectName, List<AspectGraphItem> aspectGraph) throws IOException {

        File outputFile = new File(directory + "/" + aspectName + ".dot");
        BufferedWriter output = new BufferedWriter(new FileWriter(outputFile));
        output.write("digraph " + aspectName + " {\r\n");

        for (AspectGraphItem aspectGraphItem : aspectGraph) {
            for (AspectGraphItem graphItem : aspectGraphItem.getLinksTo()) {
                output.write("\"" + aspectGraphItem.getGlossaryItem().getTerm() + "\" -> \""
                        + graphItem.getGlossaryItem().getTerm() + "\";\r\n");
            }
        }
        output.write("}");
        output.close();


    }
}
