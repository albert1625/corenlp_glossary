package utils;

import glossary.Glossary;
import glossary.GlossaryItem;

import java.io.File;
import java.io.FileNotFoundException;
import java.net.URL;
import java.util.*;

public class Utils {

    public Utils() {
    }

    public Glossary createFilledGlossary(String resourceName) throws FileNotFoundException {

        Glossary glossary = new Glossary();
        File file = getFileFromResource(resourceName);

        //reading file / filling glossary
        Scanner scanner = new Scanner(file);
        String term = null;
        GlossaryItem glossaryItem = null;
        while (scanner.hasNextLine()) {
            String line = scanner.nextLine();
            if (line.startsWith("@")) {
                term = line.substring(1);
                glossaryItem = new GlossaryItem();
                glossaryItem.setTerm(term);
            }
            else if (line.startsWith("Synonyms:")){
                List<String> synonyms = new ArrayList<>(
                        Arrays.asList(line.substring(10).split(", "))
                );
                assert glossaryItem != null;
                glossaryItem.getSynonyms().addAll(synonyms);
            }
            else if (line.startsWith("See Also:")){
                List<String> seeAlso = new ArrayList<>(
                        Arrays.asList(line.substring(10).split(", "))
                );
                assert glossaryItem != null;
                glossaryItem.getSeeAlso().addAll(seeAlso);
            }
            else if (line.equals("")){
                glossary.getTerms().put(term, glossaryItem);
            }
            else if (!line.startsWith("Ref:")){
                assert glossaryItem != null;
                glossaryItem.setDefinition(line);
            }

        }

        return glossary;
    }

    public Set<String> createSetFromFile(String resourceName) throws FileNotFoundException {
        File file = getFileFromResource(resourceName);

        Set<String> result = new HashSet<>();

        Scanner scanner = new Scanner(file);
        while (scanner.hasNextLine()) {
            String line = scanner.nextLine();
            result.add(line);
        }

        return result;
    }

    public File getFileFromResource(String resourceName){
        URL resource = getClass().getClassLoader().getResource(resourceName);
        File file;
        if (resource != null)
            file = new File(resource.getFile());
        else
            throw new IllegalArgumentException("Resource not found");

        return file;
    }

    public static List<String> getWords(List<String> sentences){
        List<String> words = new ArrayList<>();
        for (String sentence : sentences) {
            words.addAll(Arrays.asList(sentence.split(" ")));
        }
        return words;
    }

}
